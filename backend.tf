terraform {
  backend "s3" {
    bucket = "mystatebuckerterraform1409"
    key    = "state"
    region = "eu-central-1"
    dynamodb_table = "backend1409"
  }
}
