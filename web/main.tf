#ec2 

resource "aws_instance" "server" {
    ami = "ami-0a23a9827c6dab833" //
    instance_type = "t2.micro"
    subnet_id = var.sn
    security_groups = [var.sg]

    tags = {
        Name = "myserver"
    }
}